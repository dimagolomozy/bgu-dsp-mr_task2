import com.sun.tools.corba.se.idl.ExceptionEntry;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import types.MyKey;
import types.MyValue;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

public class Step2
{
    /**
     * Input:
     *      The output of the phase1
     *      Template:
     *              decade TAB n-gram TAB occurrences TAB c(w1) TAB c(w2)
     *      Example:
     *              1990    si sisylana     4   7   0
     *
     * Output:
     *      Foreach line in the input, create 3 new lines, one with the n-gram word as is, one with the first word*,
     *      and one two stars word "**"
     *      Template:
     *              decade TAB n-gram TAB occurrences TAB c(w1) TAB c(w2)
     *      Example:
     *              1990    si sisylana     4   7   0
     *              1990    si *            4   0   0
     *              1990    **              4   0   0
     */
    private static class myMapper extends Mapper<LongWritable, Text, MyKey, MyValue>
    {
        private MyKey myKey1 = new MyKey();
        private MyKey myKey2 = new MyKey();
        private MyKey myKey3 = new MyKey();
        private MyValue myValue = new MyValue();

        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException,  InterruptedException
        {
            StringTokenizer line = new StringTokenizer(value.toString());

            // get the decade
            int decade = Integer.parseInt(line.nextToken());

            // get the 2 words
            String w1 = line.nextToken();
            String w2 = line.nextToken();

            // get the occurrence of the 2gram words
            long occurrences = Long.parseLong(line.nextToken());
            myValue.setOccurrences(occurrences);

            // get the w1 count of the 2gram word
            long cW1 = Long.parseLong(line.nextToken());
            myValue.setcW1(cW1);

            // insert the 2 words as is
            myKey1.set(decade, w1 + " " + w2);
            context.write(myKey1, myValue);

            // insert first word with " *"
            myKey2.set(decade, w1 + " *");
            context.write(myKey2, myValue);

            // insert "**"
            myKey3.set(decade, "**");
            context.write(myKey3, myValue);
        }
    }

    /**
     * Combiner all the occurrences and c(w1) in the output of the mapper by the same key
     */
    private static class myCombiner extends Reducer<MyKey, MyValue, MyKey, MyValue> {

        private MyValue myValue = new MyValue();

        @Override
        public void reduce(MyKey key, Iterable<MyValue> values, Context context) throws IOException,  InterruptedException {
            long sumOccu = 0;
            long sumCw1 = 0;
            for (MyValue value : values) {
                sumOccu += value.getOccurrences();
                sumCw1 += value.getcW1();
            }
            myValue.setOccurrences(sumOccu);
            myValue.setcW1(sumCw1);
            context.write(key, myValue);
        }
    }

    /**
     * Input:
     *      The input is the sorted output of the mapper (after the combiner)
     *      Maybe output from different mappers.
     *      Template:
     *              decade TAB n-gram TAB occurrences TAB c(w1) TAB c(w2)
     *      Example:
     *              1990    **              5   0   0
     *              1990    si *            5   0   0
     *              1990    si *            4   0   0
     *              1990    si sisylana     3   7   0
     *
     * Output:
     *      Combines all the same data by key.
     *      First combines the word*'s and then all the ngram words that start with the first word.
     *      The output is the ngram words reversed! means the original n-gram with its npmi
     *
     *      Template:
     *              decade TAB n-gram TAB npmi
     *      Example:
     *              1990    analysis is     0.3
     */
    private static class myReducer extends Reducer<MyKey, MyValue, MyKey, DoubleWritable>
    {
        private MyKey myKey = new MyKey();
        private long sumOccu;
        private long sumCw1;
        private long sumCw2;
        private long decadeN;

        @Override
        public void reduce(MyKey key, Iterable<MyValue> values, Context context)
                throws IOException,  InterruptedException
        {
            // if the key is "**"
            if (key.isTwoStar())
            {
                decadeN = 0;
                for (MyValue value : values) {
                    decadeN += value.getOccurrences();
                }
            } else if (key.isWordStar())
            {   // if the key is word* (means its the w2*)
                sumCw2 = 0;
                for (MyValue value : values) {
                    sumCw2 += value.getOccurrences();
                }
            }
            else // regular 2 word ngram
            {
                sumOccu = 0;
                sumCw1 = 0;
                for (MyValue value : values) {
                    sumOccu += value.getOccurrences();
                    sumCw1 += value.getcW1();
                }

                // calculate the damn npmi
                double npmi = calculateNpmi((double)sumOccu, (double)sumCw1, (double)sumCw2, (double)decadeN);

                // set up string builder to reverse back the ngram
                StringBuilder stringBuilder = new StringBuilder(key.getWord());

                // set the key with the original ngram
                myKey.set(key.getDecade(), stringBuilder.reverse().toString());

                // write the ngram with its npmi value
                context.write(myKey, new DoubleWritable(npmi));
            }
        }

        private double calculateNpmi(double sumOccu, double sumCw1, double sumCw2, double N)
        {
            try
            {
                double pmi = Math.log(sumOccu) + Math.log(N) - Math.log(sumCw1) - Math.log(sumCw2);
                double p = sumOccu / N;
                double tmpNpmi = -1 * (pmi / Math.log(p));

                // take only 6 decimal digits
                DecimalFormat decimalFormat = new DecimalFormat("##.000000");
                String npmi = decimalFormat.format(tmpNpmi);

                return Double.parseDouble(npmi);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                System.err.println("caclulate npmi error." +
                        "\n sumOcc=" + sumOccu +
                        "\n sumCw1=" + sumCw1 +
                        "\n sumCw2=" + sumCw2 +
                        "\n N=" + N);
                return 0;
            }
        }
    }

    /**
     * Partitioner class, chooses partition base on the key hashCode() % numPartitions
     * all the same decade will go the the same reducer!
     */
    private static class myPartitioner extends Partitioner<MyKey, MyValue>
    {
        @Override
        public int getPartition(MyKey key, MyValue value, int numPartitions)
        {
            return (key.hashCode() % numPartitions);
        }
    }

    /**
     * Main of Step 2 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - mappers amount
     *            args[3] - reducers amount
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        Configuration conf = new Configuration();
        conf.set("mapreduce.job.maps", args[2]);
        conf.set("mapreduce.job.reduces", args[3]);

        Job job = new Job(conf, "Step 2");
        job.setJarByClass(Step2.class);
        job.setMapperClass(Step2.myMapper.class);
        job.setPartitionerClass(Step2.myPartitioner.class);
        job.setCombinerClass(Step2.myCombiner.class);
        job.setReducerClass(Step2.myReducer.class);
        job.setOutputKeyClass(MyKey.class);
        job.setOutputValueClass(MyValue.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
