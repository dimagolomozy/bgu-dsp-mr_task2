package types;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MyKey implements WritableComparable<MyKey>
{
    private int decade;
    private String word;

    public MyKey() {}

    public void set(int decade, String word)
    {
        this.decade = decade;
        this.word = word;
    }

    public int getDecade()
    {
        return decade;
    }

    public String getWord()
    {
        return word;
    }

    @Override
    public int compareTo(MyKey other)
    {
        int thisDecade = this.decade;
        int otherDecade = other.decade;

        if (thisDecade != otherDecade)
            return (thisDecade < otherDecade ? -1 : 1);
        else
            return this.word.compareTo(other.word);
    }

    @Override
    public void write(DataOutput out) throws IOException
    {
        out.writeInt(decade);
        out.writeUTF(word);
    }

    @Override
    public void readFields(DataInput in) throws IOException
    {
        decade = in.readInt();
        word = in.readUTF();
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (!(obj instanceof MyKey))
            return false;

        MyKey other = (MyKey)obj;

        if (this.decade != other.decade)
            return false;

        if (this.word != null && other.word != null && !this.word.equals(other.word))
            return false;

        return true;
    }

    @Override
    public String toString()
    {
        return decade + "\t" + word;
    }

    @Override
    public int hashCode()
    {
        // the decade EVEN. so we divide by 10, to get the 3 numbers of the decade. 1920 / 10 -> 192
        // and this number we multiply by prom 31
        return 31 * (decade / 10);
    }

    public boolean isWordStar()
    {
        return (!word.contains("**") && word.contains("*"));
    }

    public boolean isTwoStar()
    {
        return word.contains("**");
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        MyKey newKey = new MyKey();
        newKey.set(this.decade, this.word);

        return newKey;
    }

}
