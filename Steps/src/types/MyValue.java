package types;


import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class MyValue implements WritableComparable<MyValue>
{
    private long occurrences;
    private long cW1;
    private long cW2;

    public MyValue()
    {
        this.occurrences = 0;
        this.cW1 = 0;
        this.cW2 = 0;
    }

    public long getOccurrences()
    {
        return occurrences;
    }

    public void setOccurrences(long occurrences)
    {
        this.occurrences = occurrences;
    }

    public long getcW1()
    {
        return cW1;
    }

    public void setcW1(long cW1)
    {
        this.cW1 = cW1;
    }

    public long getcW2()
    {
        return cW2;
    }

    public void setcW2(long cW2)
    {
        this.cW2 = cW2;
    }

    @Override
    public int compareTo(MyValue other)
    {
        if (this.occurrences == other.occurrences && this.cW1 == other.cW1 && this.cW2 == other.cW2)
            return 0;

        return 1;   // don't really care
    }

    @Override
    public void write(DataOutput out) throws IOException
    {
        out.writeLong(occurrences);
        out.writeLong(cW1);
        out.writeLong(cW2);
    }

    @Override
    public void readFields(DataInput in) throws IOException
    {
        this.occurrences = in.readLong();
        this.cW1 = in.readLong();
        this.cW2 = in.readLong();
    }

    @Override
    public String toString()
    {
        return occurrences + "\t" + cW1 + "\t" + cW2;
    }

    @Override
    public int hashCode()
    {
        return (int)(occurrences * cW1 * cW2 * 7);
    }


    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;

        if (!(obj instanceof MyValue))
            return false;

        MyValue other = (MyValue)obj;

        if ( (this.occurrences != other.occurrences) || (this.cW1 != other.cW1) || (this.cW2 != other.cW2) )
            return false;

        return true;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        MyValue newValue = new MyValue();
        newValue.setOccurrences(this.occurrences);
        newValue.setcW1(this.cW1);
        newValue.setcW2(this.cW2);

        return newValue;
    }

}
