import org.apache.hadoop.io.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import types.MyValue;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;

public class Step4
{
    /**
     * Input:
     *      The input is the output of Step 3
     *      Template:
     *              decade TAB n-gram TAB npmi
     *      Example:
     *              1990    analysis is     0.2
     *
     * Output:
     *      Same data, but different key and and value
     *      Template:
     *              decade nmpi TAB n-gram
     *      Example:
     *              1990 0.2    analysis is
     *
     */
    private static class myMapper extends Mapper<LongWritable, Text, Text, Text>
    {
        private Text newKey = new Text();
        private Text newValue = new Text();

        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException,  InterruptedException
        {
            StringTokenizer line = new StringTokenizer(value.toString());

            // get the decade
            int decade = Integer.parseInt(line.nextToken());

            // get the 2 words
            String w1 = line.nextToken();
            String w2 = line.nextToken();

            // get the npmi of the 2gram words
            double npmi = Double.parseDouble(line.nextToken());

            // set the new key and value
            newKey.set(decade + " " + npmi);
            newValue.set(w1 + " " + w2);

            // insert the 2 words as is
            context.write(newKey, newValue);
        }
    }


    /**
     * Input:
     *      The output of the mapper
     *      Template:
     *              decade nmpi TAB n-gram
     *      Example:
     *              1990 0.2    analysis is
     *              1990 0.1    dima is
     *              1980 0.5    the man
     *
     * Output:
     *      Sorted by decade and npmi
     *      Template:
     *              decade nmpi TAB n-gram
     *      Example:
     *              1980 0.5    the man
     *              1990 0.1    dima is
     *              1990 0.2    analysis is
     *
     */
    private static class myComparator extends WritableComparator
    {
        protected myComparator()
        {
            super(Text.class, true);
        }

        @Override
        public int compare(WritableComparable a, WritableComparable b)
        {
            // cast the args
            Text first = (Text)a;
            Text second = (Text)b;

            // split the args by space
            String[] splitsFirst = first.toString().split(" ");
            String[] splitsSecond = second.toString().split(" ");

            int decadeFirst = Integer.parseInt(splitsFirst[0]);
            int decadeSecond = Integer.parseInt(splitsSecond[0]);

            if (decadeFirst != decadeSecond)
            {
                return (decadeFirst < decadeSecond ? -1 : 1);
            }
            else
            {
                double npmiFirst = Double.parseDouble(splitsFirst[1]);
                double npmiSecond = Double.parseDouble(splitsSecond[1]);

                if (npmiFirst != npmiSecond)
                    return (npmiFirst > npmiSecond ? -1 : 1);
                else
                    return 0;
            }
        }
    }


    /**
     * Input:
     *      The input is the SORTED! output of the mapper
     *      Template:
     *              decade nmpi TAB n-gram
     *      Example:
     *              1990 0.2    analysis is
     *
     * Output:
     *      Will output the same data, but sorted with decade and npmi
     *      will also make the key value normal again
     *      Template:
     *              decade TAB n-gram TAB npmi
     *      Example:
     *              1990    analysis is     0.2
     *
     *
     */
    private static class myReducer extends Reducer<Text, Text, Text, Text>
    {
        private Text newKey = new Text();
        private Text newValue = new Text();

        @Override
        public void reduce(Text key, Iterable<Text> values, Context context)
                throws IOException,  InterruptedException
        {
            // split the key by " " - space
            // str[0]=decade, str[1]=npmi
            String[] str = key.toString().split(" ");

            String ngram = values.iterator().next().toString();

            //set the new key and value
            newKey.set(str[0] + " " + ngram);
            newValue.set(str[1]);

            context.write(newKey, newValue);
        }

    }

    /**
     * Main of Step 4 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - mappers amount
     *            args[3] - reducers amount
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        Configuration conf = new Configuration();
        conf.set("mapreduce.job.maps", args[2]);
        conf.set("mapreduce.job.reduces", args[3]);

        Job job = new Job(conf, "Step 4");
        job.setJarByClass(Step4.class);
        job.setSortComparatorClass(Step4.myComparator.class);
        job.setMapperClass(Step4.myMapper.class);
        job.setReducerClass(Step4.myReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileInputFormat.setInputDirRecursive(job, true);
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
