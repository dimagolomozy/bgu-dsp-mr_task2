import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import types.MyKey;
import types.MyValue;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.StringTokenizer;

public class Step1
{
    private final static String stopWordsIndication_ConfKey = "stopWordsIndication";
    private final static String stopWordsCacheFileName = "stopWordsFile";

    /**
     * Input:
     *      Google 2gram database
     *      Template:
     *              n-gram TAB year TAB occurrences TAB pages TAB books
     *      Example:
     *              analysis is     1991    3   2   1
     *
     * Output:
     *      Foreach line in the input, create 2 new lines, one with the n-gram word, and one with the first word*
     *      Template:
     *              decade TAB n-gram TAB occurrences TAB c(w1) TAB c(w2)
     *      Example:
     *              1990    analysis is     3   0   0
     *              1990    analysis *      3   0   0
     */
    private static class myMapper extends Mapper<LongWritable, Text, MyKey, MyValue>
    {
        private final HashSet<String> stopWords = new HashSet<>();
        private int useStopWords = 0;
        private MyKey myKey1 = new MyKey();
        private MyKey myKey2 = new MyKey();
        private MyValue myValue = new MyValue();

        /**
         * Reads the lines from the uri and put each word to HashSet stopWords
         * @param filename - The cached file name
         * @throws IOException
         */
        private void readStopWords(String filename) throws IOException
        {
            String lines = FileUtils.readFileToString(new File(filename));
            StringTokenizer word = new StringTokenizer(lines);

            while (word.hasMoreTokens())
                stopWords.add(word.nextToken().toLowerCase());
        }

        @Override
        protected void setup(Context context) throws IOException, InterruptedException
        {
            super.setup(context);

            // get the stopWords indication
            useStopWords = Integer.parseInt(context.getConfiguration().get(stopWordsIndication_ConfKey, "0"));
            if (useStopWords != 0)
            {
                // get the files
                URI[] cacheFiles = context.getCacheFiles();

                if (cacheFiles != null && cacheFiles.length > 0)
                {
                    for (URI cachePathUri : cacheFiles)
                    {
                        Path cachePath = new Path(cachePathUri.getFragment());
                        if (cachePath.getName().equals(stopWordsCacheFileName))
                        {
                            // read stop words from file
                            readStopWords(cachePath.toString());
                            new File(cachePath.toString()).delete();
                            break;
                        }
                    }
                }
            }

        }

        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException,  InterruptedException
        {
            StringTokenizer line = new StringTokenizer(value.toString());

            // get the 2 words
            String w1 = parseWord(line.nextToken());
            String w2 = parseWord(line.nextToken());

            // check if the w1 or w2 are empty string = were not real words
            if (w1.isEmpty() || w2.isEmpty())
                return;

            // check if we are using stop words, if so, check if w1 or w2 are in the stop word collection
            if (useStopWords != 0)
                if (stopWords.contains(w1) || stopWords.contains(w2))
                    return;

            // get the year and round it down to decade 1994 -> 1990
            int year = Integer.parseInt(line.nextToken());
            year /= 10;
            year *= 10;

            // get the occurrence of the 2gram words
            long occurrences = Long.parseLong(line.nextToken());
            myValue.setOccurrences(occurrences);


            // insert the 2 words
            myKey1.set(year, w1 + " " + w2);
            context.write(myKey1, myValue);

            // insert first word with " *"
            myKey2.set(year, w1 + " *");
            context.write(myKey2, myValue);
        }

        private String parseWord(String word)
        {
            // make lower case
            String newWord = word.toLowerCase();
            // remove anything but chars
            newWord = newWord.replaceAll("[^a-z\\u05D0-\\u05EA]", "");

            // if the new word is empty? return null
            if (newWord.isEmpty())
                return "";

            return newWord;
        }
    }

    /**
     * Combiner all the occurrences in the output of the mapper by the same key
     */
    private static class myCombiner extends Reducer<MyKey, MyValue, MyKey, MyValue> {

        private MyValue myValue = new MyValue();

        @Override
        public void reduce(MyKey key, Iterable<MyValue> values, Context context) throws IOException,  InterruptedException {
            long sumOccu = 0;
            for (MyValue value : values) {
                sumOccu += value.getOccurrences();
            }
            myValue.setOccurrences(sumOccu);
            context.write(key, myValue);
        }
    }

    /**
     * Input:
     *      The input is the sorted output of the mapper (after the combiner)
     *      Maybe output from different mappers.
     *      Template:
     *              decade TAB n-gram TAB occurrences TAB c(w1) TAB c(w2)
     *      Example:
     *              1990    analysis *      3   0   0
     *              1990    analysis *      4   0   0
     *              1990    analysis is     3   0   0
     *              1990    analysis is     1   0   0
     *
     * Output:
     *      Combines all the same data by key.
     *      First combines the word*'s and then all the ngram words that start with the first word.
     *      The output is the ngram words reversed! with c(w1) not 0.
     *
     *      Template:
     *              decade TAB n-gram(reversed) TAB occurrences TAB c(w1) TAB c(w2)
     *      Example:
     *              1990    si sisylana     4   7   0
     */
    private static class myReducer extends Reducer<MyKey, MyValue, MyKey, MyValue>
    {
        private MyValue myValue = new MyValue();
        private MyKey myKey = new MyKey();

        @Override
        public void reduce(MyKey key, Iterable<MyValue> values, Context context)
                throws IOException,  InterruptedException
        {
            // if its a word* key
            if (key.isWordStar())
            {
                // sum all the occurrences of w1
                long sumCw1 = 0;
                for (MyValue value : values) {
                    sumCw1 += value.getOccurrences();
                }
                myValue.setcW1(sumCw1);
            }
            else // regular 2 word n-gram
            {
                // sum all the occuurences of the ngram
                long sumOccu = 0;
                for (MyValue value : values) {
                    sumOccu += value.getOccurrences();
                }
                myValue.setOccurrences(sumOccu);

                // create string builder to reverse the key
                StringBuilder stringBuilder = new StringBuilder(key.getWord());

                // set a new key, with reversed ngram
                myKey.set(key.getDecade(), stringBuilder.reverse().toString());

                context.write(myKey, myValue);
            }

        }
    }

    /**
     * Partitioner class, chooses partition base on the key hashCode() % numPartitions
     * all the same decade will go the the same reducer!
     */
    private static class myPartitioner extends Partitioner<MyKey, MyValue>
    {
        @Override
        public int getPartition(MyKey key, MyValue value, int numPartitions)
        {
            return (key.hashCode() % numPartitions);
        }
    }


    /**
     * Main of Step 1 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - stop words file in S3
     *            args[3] - stop words indication (0/1)
     *            args[4] - mappers amount
     *            args[5] - reducers amount
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        Configuration conf = new Configuration();
        conf.set(stopWordsIndication_ConfKey, args[3]);
        conf.set("mapreduce.job.maps", args[4]);
        conf.set("mapreduce.job.reduces", args[5]);

        Job job = new Job(conf, "Step 1");
        if (!args[3].equals("0"))
            job.addCacheFile(new URI(args[2] + "#" + stopWordsCacheFileName));
        job.setJarByClass(Step1.class);
        job.setMapperClass(Step1.myMapper.class);
        job.setPartitionerClass(Step1.myPartitioner.class);
        job.setCombinerClass(Step1.myCombiner.class);
        job.setReducerClass(Step1.myReducer.class);
        job.setOutputKeyClass(MyKey.class);
        job.setOutputValueClass(MyValue.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        SequenceFileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
