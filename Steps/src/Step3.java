import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import types.MyKey;
import types.MyValue;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.StringTokenizer;

public class Step3
{
    public final static String minPmi_ConfKey = "minPmi";
    public final static String relMinPmi_ConfKey = "relMinPmi";

    /**
     * Input:
     *      The output of the phase2 after full processing
     *      Template:
     *              decade TAB n-gram TAB npmi
     *      Example:
     *              1990    analysis is     npmi
     *
     * Output:
     *      Foreach line in the input, create 2 new lines, one with the n-gram word as is, and one with 2 start word "**"
     *      Template:
     *              decade TAB n-gram TAB npmi
     *      Example:
     *              1990    analysis is     0.5
     *              1990    **              0.5
     */
    private static class myMapper extends Mapper<LongWritable, Text, MyKey, DoubleWritable>
    {
        private MyKey myKey = new MyKey();

        @Override
        public void map(LongWritable key, Text value, Context context)
                throws IOException,  InterruptedException
        {
            StringTokenizer line = new StringTokenizer(value.toString());

            // get the decade
            int decade = Integer.parseInt(line.nextToken());

            // get the 2 words
            String w1 = line.nextToken();
            String w2 = line.nextToken();

            // get the npmi of the 2gram words
            double npmi = Double.parseDouble(line.nextToken());

            // insert the 2 words as is
            myKey.set(decade, w1 + " " + w2);
            context.write(myKey, new DoubleWritable(npmi));

            // insert 2 star word "**"
            myKey.set(decade, "**");
            context.write(myKey, new DoubleWritable(npmi));
        }
    }

    /**
     * Combiner all the npmi in the output of the mapper by the same key basically for ** word
     */
    private static class myCombiner extends Reducer<MyKey, DoubleWritable, MyKey, DoubleWritable> {

        private MyValue myValue = new MyValue();

        @Override
        public void reduce(MyKey key, Iterable<DoubleWritable> values, Context context) throws IOException,  InterruptedException
        {
            double totalNpmi = 0;
            for (DoubleWritable value : values) {
                totalNpmi += value.get();
            }
            context.write(key, new DoubleWritable(totalNpmi));
        }
    }

    /**
     * Input:
     *      The input is the sorted output of the mapper (after the combiner)
     *      Maybe output from different mappers.
     *      Template:
     *              decade TAB n-gram TAB npmi
     *      Example:
     *              1990    **              0,2
     *              1990    **              0.1
     *              1990    analysis is     0.5
     *
     * Output:
     *      Combines all the same data by key.
     *      First combines the "**" and then all the ngram words.
     *      The output is the ngram words that are Collocations.
     *
     *      Template:
     *              decade TAB n-gram TAB npmi
     *      Example:
     *              1990    analysis is     0.2
     */
    private static class myReducer extends Reducer<MyKey, DoubleWritable, MyKey, DoubleWritable>
    {

        private double decadeNpmi;

        @Override
        public void reduce(MyKey key, Iterable<DoubleWritable> values, Context context)
                throws IOException,  InterruptedException
        {

            // if the key is "**"
            if (key.isTwoStar())
            {
                decadeNpmi = 0;
                for (DoubleWritable value : values) {
                    decadeNpmi += value.get();
                }
            }
            else // regular 2 word ngram
            {
                double npmi = 0;
                for (DoubleWritable value : values) {
                    npmi += value.get();
                }

                if (isCollocation(npmi, context))
                    context.write(key, new DoubleWritable(npmi));
            }
        }

        private boolean isCollocation(double npmi, Context context) throws IOException, InterruptedException
        {
            double minPmi = Double.parseDouble(context.getConfiguration().get(minPmi_ConfKey, "0.5"));
            if (npmi >= minPmi)
                return true;

            if (decadeNpmi == 0)
                return false;

            double relMinPmi = Double.parseDouble(context.getConfiguration().get(relMinPmi_ConfKey, "0.2"));
            return ((npmi / decadeNpmi) >= relMinPmi);

        }
    }

    /**
     * Partitioner class, chooses partition base on the key hashCode() % numPartitions
     * all the same decade will go the the same reducer!
     */
    private static class myPartitioner extends Partitioner<MyKey, DoubleWritable>
    {
        @Override
        public int getPartition(MyKey key, DoubleWritable value, int numPartitions)
        {
            return (key.hashCode() % numPartitions);
        }
    }

    /**
     * Main of Step 3 jar
     * @param args: The input is correct.
     *            args[0] - input
     *            args[1] - output
     *            args[2] - minPmi
     *            args[3] - relMinPmi
     *            args[4] - mappers amount
     *            args[5] - reducers amount
     * @throws Exception
     */
    public static void main(String args[]) throws Exception
    {
        Configuration conf = new Configuration();
        conf.set(minPmi_ConfKey, args[2]);
        conf.set(relMinPmi_ConfKey, args[3]);
        conf.set("mapreduce.job.maps", args[4]);
        conf.set("mapreduce.job.reduces", args[5]);

        Job job = new Job(conf, "Step 3");
        job.setJarByClass(Step3.class);
        job.setMapperClass(Step3.myMapper.class);
        job.setPartitionerClass(Step3.myPartitioner.class);
        job.setCombinerClass(Step3.myCombiner.class);
        job.setReducerClass(Step3.myReducer.class);
        job.setOutputKeyClass(MyKey.class);
        job.setOutputValueClass(DoubleWritable.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
