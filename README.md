# **Distributed System Programming: Scale Out with Cloud Computing and Map-Reduce - 2015/Spring** #
**Collocation Extraction, by: Dima Golomozy & Eyal Ben-Simon **

## PART 1 ##

A map reduce job for collocation extraction in the 2-grams Hebrew and English corpora, for each decade.

Run command:
	java -jar ExtractCollotions <"eng"/"heb"> minPmi relMinPmi <0/1>
	
will start 3 steps as follows:

*Step 1.*

Input:
	Google 2gram database
	
	Template:
		2gram TAB year TAB occurrences TAB pages TAB books
	Example:
		analysis is     1991    3   2   1

Output:
	Decade with reversed 2gram word as key. 
	c(2gram word) and c(w1) calculated as Value.
	
	Template:
		decade TAB 2gram(reversed) TAB occurrences TAB c(w1) TAB c(w2)
	Example:
		1990    si sisylana     4   7   0


*Step 2.*

Input:
	Same as output of Step 1.

Output:
	Decade with normal 2gram word as key.
	npmi(2gram word) calculated as value.
	
	Template:
		decade TAB 2gram TAB npmi
	Example:
		1990    analysis is     0.3


*Step 3.*

Input:
	same as output of Step 2.

Output:
	Will output only 2gram words that the nmpi is:
		(npmi >= minPmi) or ((npmi / decadeNpmi) >= relMinPmi)
	Decade with 2gram word as key.
	npmi(2gram word) calculated as value.

	Template:
		decade TAB n-gram TAB npmi
	Example:
		1990    analysis is     0.2


## PART 2 ##

Run Command:
	java -jar Combine <inputFolderName - OPTIONAL>
	
This will start Step 4 and combine all the folder "result" in S3

*Step 4.*

Input:
	1 or more files, of the output of Step 3.
	
	Template:
         decade TAB n-gram TAB npmi
	Example:
         1990    analysis is     0.2
         1990	 dima is		 0.1
         1980    the man		 0.5
	

Output:
	Generates four lists altogether: Hebrew and English, with and without stop words.
	Each list includes: for each decade, collocations and there npmi value, ordered by npmi (descending).
	
	Template:
         decade TAB n-gram TAB npmi
	Example:
	  	 1980    the man		 0.5
		 1990	 dima is		 0.1
         1990    analysis is     0.2


