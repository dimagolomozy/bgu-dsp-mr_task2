import com.amazonaws.regions.Regions;

public final class Common
{
    // Amazon data
    public static final String awsCredentials = "AwsCredentials.properties";
    public static final String SSH_KEY = "AWS_Eyal_Dima";

    // Instance properties
    public static final String HADOOP_VERSION = "2.4.0";
    public static final String INSTANCE_PLACEMENT = "us-east-1c";

    // S3 Directories
    public static final String S3_BUCKET_NAME = "dima-eyal-bucket-task2";
    public static final String S3_JAR_DIR = "s3://" + S3_BUCKET_NAME + "/jars/";
    public static final String S3_RESULT_DIR = "s3://" + S3_BUCKET_NAME + "/result/";
    public static final String S3_INPUT_DIR = "s3://" + S3_BUCKET_NAME + "/input/";
    public static final String S3_STOPWORDS_DIR = "s3://" + S3_BUCKET_NAME + "/stopwords/";
    public static final String S3_LOG_DIR = "s3://" + S3_BUCKET_NAME + "/log/";
    public static final String S3_STEPS_OUTPUT_DIR = "s3://" + S3_BUCKET_NAME + "/stepsOutput/";

    // Elastic MapReduce
    public static final Regions EMR_REGION = Regions.US_EAST_1;
}
