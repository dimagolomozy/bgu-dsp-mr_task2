import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.*;

import java.io.InputStream;
import java.util.UUID;

public class Combine
{
    // Instance properties
    private static final int INSTANCE_COUNT = 2;
    private static final String INSTANCE_TYPE = InstanceType.M1Large.toString();

    // Flow properties
    private static final UUID RANDOM_UUID = UUID.randomUUID();
    private static final String FLOW_NAME = "flow-Combine" + RANDOM_UUID.toString();
    private static final String SERVICE_ROLE = "EMR_DefaultRole";
    private static final String FLOW_ROLE = "EMR_EC2_DefaultRole";
    private static final String MAPPERS_AMOUNT = "5";
    private static final String REDUCERS_AMOUNT = "1";

    // S3 Directories
    private static final String S3_FLOW_RESULT_DIR = Common.S3_RESULT_DIR + FLOW_NAME + "/";
    private static final String S3_FLOW_LOG_DIR = Common.S3_LOG_DIR + FLOW_NAME + "/";

    // Elastic MapReduce
    private static AmazonElasticMapReduceClient emr;


    /**
     * main function - starting point
     * @param args :
     * 		args[0] = the name of the input folder/file	- OPTIONAL
     * 	              its a recursive function. so base dir is also acceptable
     */
    public static void main(String[] args)
    {
        try
        {
            String input = checkAndGetArgs(args);

            // init the amazon EMR client
            initEMR();

            // initAmazon steps 4
            StepConfig stepConfig = initStep(input);

            // run the cluster with the steps
            runCluster(stepConfig);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static String checkAndGetArgs(String[] args) throws Exception
    {
        if (args.length > 1)
        {
            throw new Exception("Invalid usage: <inputFolderName - OPTIONAL>");
        }
        else if (args.length == 1)
        {
            return args[0];
        }

        return Common.S3_RESULT_DIR;
    }

    private static void initEMR() throws Exception
    {
        System.out.println("# Init EMR");
        InputStream awsCredentialInputStream = Combine.class.getResourceAsStream(Common.awsCredentials);

        AWSCredentials credentials = new PropertiesCredentials(awsCredentialInputStream);
        emr = new AmazonElasticMapReduceClient(credentials);
        System.out.println(">>> Set region " + Common.EMR_REGION.toString());
        emr.setRegion(Region.getRegion(Common.EMR_REGION));
    }

    private static StepConfig initStep(String input)
    {
        System.out.println("# Init steps");

        final String output = S3_FLOW_RESULT_DIR + "outputStep4";

        HadoopJarStepConfig hadoopJarStep = new HadoopJarStepConfig()
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass("Step4")     // step 4 in the map reduce application
                        .withArgs(input, output, MAPPERS_AMOUNT, REDUCERS_AMOUNT);


        return new StepConfig()
                .withName("Step4")
                .withHadoopJarStep(hadoopJarStep)
                .withActionOnFailure("TERMINATE_JOB_FLOW");
    }

    private static void runCluster(StepConfig stepsConfig)
    {
        System.out.println("# Init instances");
        System.out.println(">>> Count: " + INSTANCE_COUNT +
                "\n>>> Type: " + INSTANCE_TYPE +
                "\n>>> Hadoop ver: " + Common.HADOOP_VERSION +
                "\n>>> SSH key: " + Common.SSH_KEY +
                "\n>>> Placement: " + Common.INSTANCE_PLACEMENT);

        JobFlowInstancesConfig instances = new JobFlowInstancesConfig()
                .withInstanceCount(INSTANCE_COUNT)
                .withMasterInstanceType(INSTANCE_TYPE)
                .withSlaveInstanceType(INSTANCE_TYPE)
                .withHadoopVersion(Common.HADOOP_VERSION)
                .withEc2KeyName(Common.SSH_KEY)
                .withKeepJobFlowAliveWhenNoSteps(false)
                .withPlacement(new PlacementType(Common.INSTANCE_PLACEMENT));


        System.out.println("# Init job flow request");
        System.out.println(">>> Name: " + FLOW_NAME +
                "\n>>> Steps count: 1" +
                "\n>>> Log uri: " + S3_FLOW_LOG_DIR +
                "\n>>> Service role: " + SERVICE_ROLE +
                "\n>>> Job flow role: " + FLOW_ROLE);

        RunJobFlowRequest runFlowRequest = new RunJobFlowRequest()
                .withName(FLOW_NAME)
                .withInstances(instances)
                .withSteps(stepsConfig)
                .withLogUri(S3_FLOW_LOG_DIR)
                .withServiceRole(SERVICE_ROLE)
                .withJobFlowRole(FLOW_ROLE);


        System.out.println("# Sending job flow: " + runFlowRequest.getName());
        RunJobFlowResult runJobFlowResult = emr.runJobFlow(runFlowRequest);

        String jobFlowId = runJobFlowResult.getJobFlowId();
        System.out.println(">>> Ran job flow with id: " + jobFlowId);
    }
}
