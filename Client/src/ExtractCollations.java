import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.*;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class ExtractCollations
{
    // Instance properties
    private static final int INSTANCE_COUNT = 6;
    private static final String INSTANCE_TYPE = InstanceType.M1Large.toString();

    // Flow properties
    private static final UUID RANDOM_UUID = UUID.randomUUID();
//    private static final String FLOW_NAME = "flow-ENG-" + RANDOM_UUID.toString();
//    private static final String FLOW_NAME = "flow-HEB-" + RANDOM_UUID.toString();
//    private static final String FLOW_NAME = "flow-ENG-WITH-STOPWORDS-" + RANDOM_UUID.toString();
    private static final String FLOW_NAME = "flow-HEB-WITH-STOPWORDS-" + RANDOM_UUID.toString();
    private static final String SERVICE_ROLE = "EMR_DefaultRole";
    private static final String FLOW_ROLE = "EMR_EC2_DefaultRole";
    private static final String MAPPERS_AMOUNT = "10";
    private static final String REDUCERS_AMOUNT = "2";

    // S3 Directories
    private static final String S3_FLOW_RESULT_DIR = Common.S3_RESULT_DIR + FLOW_NAME + "/";
    private static final String S3_FLOW_STEPS_OUTPUT = Common.S3_STEPS_OUTPUT_DIR + FLOW_NAME + "/";
    private static final String S3_FLOW_LOG_DIR = Common.S3_LOG_DIR + FLOW_NAME + "/";
    private static final String S3_NGRAM_ENG = "s3://datasets.elasticmapreduce/ngrams/books/20090715/eng-us-all/2gram/data";
    private static final String S3_NGRAM_HEB = "s3://datasets.elasticmapreduce/ngrams/books/20090715/heb-all/2gram/data";

    // Elastic MapReduce
    private static AmazonElasticMapReduceClient emr;


    /**
     * main function - starting point
     * @param args :
     * 		args[0] = The name of the input folder or full path to the file
     * 		args[1] = the minPmi
     * 		args[2] = the relMinPmi
     * 	    args[3] = language (heb/eng)
     * 	    args[4] = include stop-words (0/1)
     */
    public static void main(String[] args)
    {
        try
        {
            // check the args for correction
            checkArgs(args);

            // init the amazon EMR client
            initEMR();

            // initAmazon steps 1, 2, 3
            List<StepConfig> stepsConfig = initSteps(args[0], args[1], args[2], args[3], args [4]);

            // run the cluster with the steps
            runCluster(stepsConfig);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private static void initEMR() throws Exception
    {
        System.out.println("# Init EMR");
        InputStream awsCredentialInputStream = ExtractCollations.class.getResourceAsStream(Common.awsCredentials);

        AWSCredentials credentials = new PropertiesCredentials(awsCredentialInputStream);
        emr = new AmazonElasticMapReduceClient(credentials);
        System.out.println(">>> Set region " + Common.EMR_REGION.toString());
        emr.setRegion(Region.getRegion(Common.EMR_REGION));
    }


    private static void runCluster(List<StepConfig> stepsConfig)
    {
        System.out.println("# Init instances");
        System.out.println(">>> Count: " + INSTANCE_COUNT +
                "\n>>> Type: " + INSTANCE_TYPE +
                "\n>>> Hadoop ver: " + Common.HADOOP_VERSION +
                "\n>>> SSH key: " + Common.SSH_KEY +
                "\n>>> Placement: " + Common.INSTANCE_PLACEMENT);

        JobFlowInstancesConfig instances = new JobFlowInstancesConfig()
                .withInstanceCount(INSTANCE_COUNT)
                .withMasterInstanceType(INSTANCE_TYPE)
                .withSlaveInstanceType(INSTANCE_TYPE)
                .withHadoopVersion(Common.HADOOP_VERSION)
                .withEc2KeyName(Common.SSH_KEY)
                .withKeepJobFlowAliveWhenNoSteps(false)
                .withPlacement(new PlacementType(Common.INSTANCE_PLACEMENT));


        System.out.println("# Init job flow request");
        System.out.println(">>> Name: " + FLOW_NAME +
                "\n>>> Steps count: " + stepsConfig.size() +
                "\n>>> Log uri: " + S3_FLOW_LOG_DIR +
                "\n>>> Service role: " + SERVICE_ROLE +
                "\n>>> Job flow role: " + FLOW_ROLE);

        RunJobFlowRequest runFlowRequest = new RunJobFlowRequest()
                .withName(FLOW_NAME)
                .withInstances(instances)
                .withSteps(stepsConfig)
                .withLogUri(S3_FLOW_LOG_DIR)
                .withServiceRole(SERVICE_ROLE)
                .withJobFlowRole(FLOW_ROLE);


        System.out.println("# Sending job flow: " + runFlowRequest.getName());
        RunJobFlowResult runJobFlowResult = emr.runJobFlow(runFlowRequest);

        String jobFlowId = runJobFlowResult.getJobFlowId();
        System.out.println(">>> Ran job flow with id: " + jobFlowId);
    }

    private static List<StepConfig> initSteps(String input, String minPmi, String relMinPmi,
                                              String language, String stopWordsIndicator)
    {
        System.out.println("# Init steps");

//        final String inputStep1 = Common.S3_INPUT_DIR + input;
        final String inputStep1 = (language.toLowerCase().equals("heb") ? S3_NGRAM_HEB : S3_NGRAM_ENG);
        final String outputStep1 = S3_FLOW_STEPS_OUTPUT + "outputStep1";
        final String outputStep2 = S3_FLOW_STEPS_OUTPUT + "outputStep2";
        final String outputStep3 = S3_FLOW_RESULT_DIR + "outputStep3";
        final String stopWordsFile = Common.S3_STOPWORDS_DIR + language.toLowerCase();

        HadoopJarStepConfig[] hadoopJarsStep = new HadoopJarStepConfig[] {
                new HadoopJarStepConfig()
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass("Step1")     // step 1 in the map reduce application
                        .withArgs(inputStep1, outputStep1, stopWordsFile, stopWordsIndicator, MAPPERS_AMOUNT, REDUCERS_AMOUNT),

                new HadoopJarStepConfig()
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass("Step2")     // step 2 in the map reduce application
                        .withArgs(outputStep1, outputStep2, MAPPERS_AMOUNT, REDUCERS_AMOUNT),

                new HadoopJarStepConfig()
                        .withJar(Common.S3_JAR_DIR + "Steps.jar")
                        .withMainClass("Step3")     // step 3 in the map reduce application
                        .withArgs(outputStep2, outputStep3, minPmi, relMinPmi, MAPPERS_AMOUNT, REDUCERS_AMOUNT),
        };


        List<StepConfig> stepsConfig = new LinkedList<>();
        stepsConfig.add(new StepConfig()
                .withName("Step1")
                .withHadoopJarStep(hadoopJarsStep[0])
                .withActionOnFailure("TERMINATE_JOB_FLOW"));

        stepsConfig.add(new StepConfig()
                .withName("Step2")
                .withHadoopJarStep(hadoopJarsStep[1])
                .withActionOnFailure("TERMINATE_JOB_FLOW"));

        stepsConfig.add(new StepConfig()
                .withName("Step3")
                .withHadoopJarStep(hadoopJarsStep[2])
                .withActionOnFailure("TERMINATE_JOB_FLOW"));

        return stepsConfig;
    }

    private static boolean checkArgs(String[] args) throws Exception
    {
        // Check arguments amount
        if (args.length != 5)
            throw new Exception("Invalid usage: <inputFolderName> <minPmi> <relMinPmi> " +
                    "<heb/eng> <stop-words(0/1)>");

        // check if minPmi good
        Double.parseDouble(args[1]);
        // check if relMinPmi good
        Double.parseDouble(args[2]);
        // check if stop-words good
        Integer.parseInt(args[4]);

        // check language = eng / heb
        String language = args[3].toLowerCase();
        if (!language.equals("heb") && (!language.equals("eng")))
            throw new Exception("Language should be eng/heb only");

        return true;
    }


}

